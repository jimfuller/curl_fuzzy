.DEFAULT_GOAL := all

python3=`which python3`
black=`which black`
radon=`which radon`
curl=`which curl`
docker=`which docker`
dockercompose=`which docker-compose`

src_dir=curl_fuzzy

export PYTHONPATH=.

export CURL_FUZZY_IGNORE_STATUS_CODES=0,1,2,4,6,7,18,22,60
export CURL_FUZZY_NUM_CASES=10000
export CURL_FUZZY_CURL_PATH=bin/run_curl.sh
export CURL_FUZZY_THREADS=4
export CURL_FUZZY_VERBOSE=1
export CURL_FUZZY_TEST_HOST_NAME=httpbin
export CURL_FUZZY_TIMEOUT=30
export CURL_FUZZY_MAX_NUM_SWITCHES=3
export CURL_FUZZY_MAX_NUM_HEADERS=3

complexity:
	 ${radon} cc ${src_dir}

lint:
	 ${python3} -m pylint --exit-zero ${src_dir}/*

format:
	${black} ${src_dir}/*.py

clean:
	rm -rf tmp
	mkdir -p tmp

test: clean
	pytest -s tests/*

setup: startDocker
	${docker} cp curl_fuzzy_http_test_webserver:/server.crt /tmp/server.crt
	${docker} cp /tmp/server.crt curl_fuzzy_test_runner:/server.crt
	${curl} https://curl.haxx.se/ca/cacert.pem > /tmp/cacert.pem
	${docker} cp /tmp/cacert.pem curl_fuzzy_test_runner:/cacert.pem
	${docker} exec curl_fuzzy_test_runner service docker start
	${docker} exec curl_fuzzy_http_test_webserver nghttpx -f0.0.0.0,443 -b127.0.0.1,8080 /server.key /server.crt &
	#${docker} exec curl_fuzzy_test_runner tc qdisc add dev eth0 root netem delay 100ms
	#${docker} exec curl_fuzzy_http_test_webserver tc qdisc add dev eth0 root netem delay 100ms

startFuzz: stopDocker setup
	${docker} exec curl_fuzzy_test_runner make test

startDocker:
	${docker} network ls | grep internal_net > /dev/null || ${docker} network create internal_net || true
	${dockercompose} up -d --remove-orphans || true

stopDocker:
	${dockercompose} down --remove-orphans || true
	${docker} network delete internal_net || true
	${docker} system prune -f || true

buildDocker:
	docker build --compress -t "jfuller/curl_fuzzy_server_httpbin" -f testServerDockerfile .
	docker build --compress -t "jfuller/curl_fuzzy_client" -f testClientDockerfile .

all: test format lint complexity

