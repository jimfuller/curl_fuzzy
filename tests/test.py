import unittest
from curl_fuzzy import curl_fuzzy


class MyTestCase(unittest.TestCase):

    def test_getting_files(self):
        corpus = curl_fuzzy.get_files()
        print(corpus)
        assert len(corpus) == 1600

    def test_run(self):
        curl_fuzzy.run_fuzz()


if __name__ == "__main__":
    unittest.main()
