[![https://gitlab.com/jimfuller/curl_fuzzy/-/pipelines](https://gitlab.com/jimfuller/curl_fuzzy/badges/master/pipeline.svg)](https://gitlab.com/jimfuller/curl_fuzzy/-/pipelines)
# curl_fuzzy

warning - this project is alpha, expeditionary code ... likely to not work as expected. 

A 'hand crafted' curl fuzzer written in 'plain old python' focused on exercising the myriad set of 
curl and http options.

The curl project already has (a very good) [automated fuzzer](https://github.com/curl/curl-fuzzer) but
I wanted to investigate how robust the curl commandline is with respect to different options modulo 
different HTTP requests.

Note - this was created with little concern for reuse (ex. for using on other command line tools).

## Dependencies

Docker-compose is used to instantiate test web server(s) and python test runner image that performs the actual
 fuzzing.
 
## Running 

Running the following will setup and start fuzzing:
```
> make startFuzz
```

Everything is controlled via a small set of env vars:

```
export CURL_FUZZY_IGNORE_STATUS_CODES=0,1,2,4,6,18,22
export CURL_FUZZY_NUM_CASES=100000
export CURL_FUZZY_CURL_PATH=curl
export CURL_FUZZY_THREADS=8
export CURL_FUZZY_VERBOSE=1
export CURL_FUZZY_TEST_HOST_NAME=httpbin
```
where 

* CURL_FUZZY_IGNORE_STATUS_CODES: the curl error codes to ignore (writing to log)
* CURL_FUZZY_NUM_CASES: the number of cases to generate and run
* CURL_FUZZY_CURL_PATH: the curl executable to test
* CURL_FUZZY_THREADS: number of threads to use 
* CURL_FUZZY_VERBOSE: controls verbosity of console output
* CURL_FUZZY_TEST_HOST_NAME: sets http test web server 

To view interesting 'hits' review tmp/error_log.