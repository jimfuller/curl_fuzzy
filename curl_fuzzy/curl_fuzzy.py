"""
"""
import signal
import subprocess, threading, time, random
import sys
import traceback

from curl_fuzzy.utils import get_files, get_random_string
from curl_fuzzy.config import (
    num_threads,
    num_cases,
    num_range_switches,
    num_range_headers,
    test_max_time,
)

start = None
cases = 0


def curl_fuzzy(thr_id, cases, inp):
    """ run fuzz in thread """
    # ensure we load module vars to get unique values
    from curl_fuzzy.config import (
        test_verbose,
        test_host_name,
        command_curl,
        commands_data,
        command_ignore_status_codes,
        commands_switches,
        commands_methods,
        commands_headers,
        commands_schemes,
    )

    assert isinstance(thr_id, int)

    cmd = list()

    cmd = [command_curl, "-v"]

    # inject method
    scheme = random.choice(commands_schemes)
    meth = random.choice(commands_methods)
    if random.choice([True, False]):
        cmd.insert(1, f"-X{meth}")

    # inject headers
    for i in range(random.choice(range(num_range_headers))):
        if random.choice([True, False]):
            cmd.append("-H")
            cmd.append(random.choice(commands_headers))

    # inject switches
    for i in range(random.choice(range(num_range_switches))):
        select_switch = random.choice(commands_switches)
        if isinstance(select_switch, str):
            cmd.insert(2, select_switch)
        elif isinstance(select_switch, list):
            cmd.insert(2, select_switch[0])
            cmd.insert(3, select_switch[1])

    if scheme == "https":
        cmd.append("--cacert")
        cmd.append("/server.crt")

    if test_max_time > 0:
        cmd.append("--max-time")
        cmd.append(f"{test_max_time}")

    # inject data based on method
    if meth in ["POST", "PUT", "PATCH"]:
        cmd.append(random.choice(commands_data))
        if "--data-urlencode" in cmd:
            cmd.append(f"http://{test_host_name}")
        elif "-F" in cmd:
            cmd.append(f"{get_random_string(50)}={get_random_string(10000)}")
        else:
            if random.choice([True, False]):
                cmd.append(f"@{inp}")
            else:
                cmd.append(f"@{inp}")
                # cmd.append("@/dev/random")

    if meth in ["GET"]:
        urls = [
            f"{get_random_string(101)}.example.org",
            f"{scheme}://{test_host_name}/{meth.lower()}"
            f"{scheme}://{test_host_name}/base64/{random.randint(1,1000)}",
            f"{scheme}://{test_host_name}/bytes/{random.randint(1,1000)}",
            f"{scheme}://{test_host_name}/delay/{random.randint(1,5)}",
            f"{scheme}://{test_host_name}/range/{random.randint(1,5)}",
            f"{scheme}://{test_host_name}/stream/{random.randint(1,10)}",
            f"{scheme}://{test_host_name}/drip",
            f"{scheme}://{test_host_name}/cache",
            f"{scheme}://{test_host_name}/cache/{random.randint(1, 5)}",
            f"{scheme}://{test_host_name}/cookies",
            f"{scheme}://{test_host_name}/image/png",
            f"{scheme}://{test_host_name}/absolute-redirect/{random.randint(1, 5)}",
            f"{scheme}://{test_host_name}/anything",
            f"{scheme}://{test_host_name}/anything/../.././../../.",
        ]
        cmd.append(random.choice(urls))
    else:
        cmd.append(f"{scheme}://{test_host_name}/{meth.lower()}")

    if test_verbose:
        print(cmd)

    # run it
    try:
        sp = subprocess.run(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        # if sp.returncode != 0:
        #     print("subprocess return code not zero")
        #     print(sp.returncode)

        out = str(sp.stdout)
        err = str(sp.stderr)

        # if test_verbose:
        #     print(str(sp.returncode))
        #     print(out)
        #     print(err)
        # print("\n-------------------------------------\n")

        tmp_fn = "tmp/error_log"

        # write to error log if interesting
        if sp.returncode == -signal.SIGSEGV:
            with open(tmp_fn, "a+") as temp_file:
                temp_file.write("\n-------------------------------------\n")
                temp_file.write("\n----------SEGMENT FAULT--------------\n")
                temp_file.write(str(sp.returncode))
                temp_file.write("   ")
                temp_file.write(str(cases))
                temp_file.write("   ")
                temp_file.write(inp)
                temp_file.write("\n")
                temp_file.write(str(cmd))
                temp_file.write("\n")
                temp_file.write(out)
                temp_file.write("\n")
                temp_file.write(err)
        if sp.returncode > 0 and sp.returncode not in command_ignore_status_codes:
            with open(tmp_fn, "a+") as temp_file:
                temp_file.write("\n-------------------------------------\n")
                temp_file.write(str(sp.returncode))
                temp_file.write("   ")
                temp_file.write(str(cases))
                temp_file.write("   ")
                temp_file.write(inp)
                temp_file.write("\n")
                temp_file.write(str(cmd))
                temp_file.write("\n")
                temp_file.write(out)
                temp_file.write("\n")
                temp_file.write(err)
        return inp
    except Exception as e:
        print("subprocess error")
        tb = traceback.format_exc()
        print(e.output)
        print(tb)
        sys.exit()


def worker(thr_id):
    """ worker thread """
    global start, cases

    while True:
        cases += 1
        result = curl_fuzzy(thr_id, cases, random.choice(corpus))
        elapsed = time.time() - start
        print(f"{thr_id} {cases}. elapsed={elapsed} | fn={result} ")
        if cases > num_cases:
            break


def run_fuzz():
    """ entrypoint for threaded running of fuzzer"""

    global start, corpus, cases

    corpus = set()
    corpus_filenames = get_files()
    for file_name in corpus_filenames:
        corpus.add(file_name)
    corpus = list(corpus)
    start = time.time()

    for thr_id in range(num_threads):
        threading.Thread(target=worker, args=[thr_id]).start()
    while threading.active_count() > 1:
        print("sleep")
        time.sleep(0.1)
