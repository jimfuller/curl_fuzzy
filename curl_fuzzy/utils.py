"""
generic utils
"""

import random, string, glob

SEPERATOR = ","


def get_random_list_of_strings(x, n):
    """ generate list of strings """
    return SEPERATOR.join([get_random_string(x)] * random.randint(1, n))


def get_random_string(length):
    """Random string with the combination of lower and upper case """
    letters = string.ascii_letters
    result_str = "".join(random.choice(letters) for i in range(length))
    return result_str


def get_files():
    """ retrive list of corpus files """
    return glob.glob("corpus/corpus/*")
